package com.example.aluno.appctof;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.security.spec.RSAOtherPrimeInfo;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void converteCelsiusToFahrenheit(View v){
        EditText v_aux = (EditText)findViewById(R.id.celsius);
        double cls = Double.parseDouble(v_aux.getText().toString());
        double resultado = (((cls * 9)/5) + 32);
        double result_toKelvin = cls + 273.15;
        TextView vw = (TextView)findViewById(R.id.TV1);
        TextView vw2 = (TextView)findViewById(R.id.TV2);
        vw.setText(resultado + "");
        vw2.setText(result_toKelvin + "");
    }
}
